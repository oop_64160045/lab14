/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatda.swingproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author chana
 */
public class WriteFriend {
    public static void main(String[] args) throws IOException{
        ArrayList<Friend>friends = new ArrayList();
        friends.add(new Friend("Worawit", 20 , "Male" , "Best Friend"));
        friends.add(new Friend("Svanee", 21 , "Female" , "Best Friend"));
        friends.add(new Friend("Tip",18 , "Female" , "Best Friend"));
        
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            file = new File("friend.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(friends);
            oos.close();
            fos.close();
        }catch(FileNotFoundException ex){
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE,null,ex);
        }catch(IOException ex){
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE,null,ex);
        }
        
    }
}
