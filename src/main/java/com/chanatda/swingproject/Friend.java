/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatda.swingproject;

import java.io.Serializable;

/**
 *
 * @author chana
 */
public class Friend implements Serializable {
    private String name;
    private int age;
    private String gender;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setDescirption(String descirption) {
        this.descirption = descirption;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public String getDescirption() {
        return descirption;
    }

    public Friend(String name, int age, String gender, String descirption) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.descirption = descirption;
    }
    private String descirption;

    @Override
    public String toString() {
        return "Friend{" + "name = " + name + ", age = " + age + ", gender = " + gender + ", descirption = " + descirption + '}';
    }
    
    
}
